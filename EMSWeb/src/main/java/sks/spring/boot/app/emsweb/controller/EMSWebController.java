package sks.spring.boot.app.emsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EMSWebController {
	
	@RequestMapping(value="/index")
	public String indexPage() {
		return "index";
	}

}
