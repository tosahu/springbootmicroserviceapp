package sks.spring.boot.app.ccc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RefreshScope
@RestController
public class CloudConfigurationClientApplication {
	
	@Value("${welcome.text}")
	private String data;

	public static void main(String[] args) {
		SpringApplication.run(CloudConfigurationClientApplication.class, args);
	}
	
	@RequestMapping("/")
	public String index() {
		return data;
	}

}
